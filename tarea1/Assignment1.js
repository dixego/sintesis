// JavaScript source code

var NUM = "NUM";
var FALSE = "FALSE";
var VR = "VAR";
var PLUS = "PLUS";
var TIMES = "TIMES";
var LT = "LT";
var AND = "AND";
var NOT = "NOT";
var ITE = "ITE";

var ALLOPS = [NUM, FALSE, VR, PLUS, TIMES, LT, AND, NOT, ITE];

function str(obj) { return JSON.stringify(obj); }

//Constructor definitions for the different AST nodes.

function flse() {
    return { type: FALSE, toString: function () { return "false"; } };
}

function vr(name) {
    return { type: VR, name: name, toString: function () { return this.name; } };
}
function num(n) {
    return { type: NUM, val: n, toString: function () { return this.val; } };
}
function plus(x, y) {
    return { type: PLUS, left: x, right: y, toString: function () { return "("+ this.left.toString() + "+" + this.right.toString()+")"; } };
}
function times(x, y) {
    return { type: TIMES, left: x, right: y, toString: function () { return "(" + this.left.toString() + "*" + this.right.toString() + ")"; } };
}
function lt(x, y) {
    return { type: LT, left: x, right: y, toString: function () { return "(" + this.left.toString() + "<" + this.right.toString() + ")"; } };
}
function and(x, y) {
    return { type: AND, left: x, right: y, toString: function () { return "(" + this.left.toString() + "&&" + this.right.toString() + ")"; } };
}
function not(x) {
    return { type: NOT, left: x, toString: function () { return "(!" + this.left.toString()+ ")"; } };
}
function ite(c, t, f) {
    return { type: ITE, cond: c, tcase: t, fcase: f, toString: function () { return "(if " + this.cond.toString() + " then " + this.tcase.toString() + " else " + this.fcase.toString() + ")"; } };
}

//Interpreter for the AST.
function interpret(exp, envt) {
    switch (exp.type) {
        case FALSE: return false;
        case NUM: return exp.val;
        case VR: return envt[exp.name];
        case PLUS: return interpret(exp.left, envt) + interpret(exp.right, envt);
        case TIMES: return interpret(exp.left, envt) * interpret(exp.right, envt);
        case LT: return interpret(exp.left, envt) < interpret(exp.right, envt);
        case AND: return interpret(exp.left, envt) && interpret(exp.right, envt);
        case NOT: return !interpret(exp.left, envt);
        case ITE: if (interpret(exp.cond, envt)) { return interpret(exp.tcase, envt); } else { return interpret(exp.fcase, envt); }
    }
}

//Some functions you may find useful:
function randInt(lb, ub) {
    var rf = Math.random();
    rf = rf * (ub - lb) + lb;
    return Math.floor(rf);
}

function randElem(from) {
    return from[randInt(0, from.length)];
}

function writeToConsole(text) {
    var csl = document.getElementById("console");
    if (typeof text == "string") {
        csl.value += text + "\n";
    } else {
        csl.value += text.toString() + "\n";
    }
}

// initialize a list of all allowed terminal symbols
// (that is: FALSE, allowed VARs and allowed NUMs)
function initializePlist(vars, consts) {
  var plist = [flse()];
  //var plist = [];
  vars.forEach(v => plist.push(vr(v)));
  consts.forEach(n => plist.push(num(n)));
  return plist;
}

// test a program for correctness according to
// examples
function isCorrect(program, examples) {
  var res = examples.map(x => interpret(program, x) === x._out);
  return !res.includes(false);
}

function equivalent(p1, p2, examples) {
  var res = examples.map(x => interpret(p1, x) === interpret(p2, x));
  return !res.includes(false);
}

function isNumerical(exp) {
  return [NUM, VR, PLUS, TIMES, ITE].includes(exp.type);
}

function isBoolean(exp) {
  return !isNumerical(exp);
}

function unary(exp, allowed) {
  var results = [];
  if(isBoolean(exp) && allowed.includes(NOT)) {
    results.push(
      not(exp)
    )
  }
  return results;
}

function binary(e1, e2, allowed, binAllowed) {
  var results = [];
  if(isNumerical(e1) && isNumerical(e2)) {
    //if(allowed.includes(PLUS))
      results.push(plus(e1, e2))
    //if(allowed.includes(TIMES))
      results.push(times(e1,e2))
    //if(allowed.includes(LT))
      results.push(lt(e1, e2), lt(e2, e1))
  }

  if(isBoolean(e1) && isBoolean(e2)) {
    if(binAllowed.includes(AND))
      results.push(and(e1, e2));
  }
  return results;
}

function ternary(e1, e2, e3, allowed) {
  var results = [];
  if(isBoolean(e1) && (isNumerical(e2) === isNumerical(e3))) {
    if(allowed.includes(ITE))
      results.push(
        ite(e1, e2, e3),
        ite(e1, e3, e2)
      );
  }
  return results;
}


function grow(plist, allowed, binAllowed) {
  // first all unary terms.
  var result = [];
  var i, j, k;
  for(i = 0; i < plist.length; i++) {
    result = result.concat(unary(plist[i], binAllowed));
  }
  // then all binary terms
  for(i = 0; i < plist.length; i++) {
    for(j = 0; j < plist.length; j++){
      result = result.concat(binary(plist[i], plist[j], allowed, binAllowed));
    }
  }
  // then all ternary terms
  for(i = 0; i < plist.length; i++) {
    for(j = 0; j < plist.length; j++){
      for(k = 0; k < plist.length; k++) {
        result = result.concat(ternary(plist[i], plist[j], plist[k], allowed));
      }
    }
  }
  return plist.concat(result);
}

function elimEquivalents(plist, examples) {
  var i, j;
  for(i = 0; i < plist.length; i++) {
    for(j = i+1; j < plist.length; j++) {
      if(equivalent(plist[i], plist[j], examples)) {
        plist.splice(j, 1);
        j--;
      }
    }
  }
  return plist;
}

function bottomUp(globalBnd, intOps, boolOps, vars, consts, inputoutputs) {
  var plist = initializePlist(vars, consts);
  console.log(plist.map(p => p.toString()));
  var i = 0, j;
  while(i < globalBnd) {
    plist = grow(plist, intOps, boolOps);
    console.log(plist.map(p => p.toString()));
    plist = elimEquivalents(plist, inputoutputs);
    console.log(plist.map(p => p.toString()));

    for(j = 0; j < plist.length; j++) {
      if(isCorrect(plist[j], inputoutputs)){
        return plist[j];
      }
    }
    i++;
  }
  return "FAIL";
}

function binaryFaster(e1, e2, allowed, binAllowed) {
  var results = [];
  if(isNumerical(e1) && isNumerical(e2)) {
    if(allowed.includes(PLUS))
      results.push(plus(e1, e2));
    if(binAllowed.includes(LT))
      results.push(lt(e1, e2), lt(e2, e1));
  }

  if(e1.type == NUM){
    if(e2.type == VR){
      if(allowed.includes(TIMES))
        results.push(times(e1, e2));
    }
    if(e2.type == NUM){
      if(binAllowed.includes(LT))
        results.push(lt(e1, e2), lt(e2, e1));
    }
  } else if(e1.type == VR){
    if(e2.type == VR || e2.type == NUM){
      if(allowed.includes(TIMES))
        results.push(times(e1, e2));
      if(binAllowed.includes(LT))
        results.push(lt(e1, e2), lt(e2, e1));
    }
  }

  if(isBoolean(e1) && isBoolean(e2)) {
    if(binAllowed.includes(AND))
      results.push(
        and(e1, e2)
      );
  }
  return results;
}

function growFaster(plist, allowed, bAllowed) {
  // first all unary terms.
  var result = [];
  var i, j, k;
  for(i = 0; i < plist.length; i++) {
    result = result.concat(unary(plist[i], allowed));
  }
  // then all binary terms
  for(i = 0; i < plist.length; i++) {
    for(j = 0; j < plist.length; j++){
      result = result.concat(binaryFaster(plist[i], plist[j], allowed, bAllowed));
    }
  }
  // then all ternary terms
  for(i = 0; i < plist.length; i++) {
    for(j = 0; j < plist.length; j++){
      for(k = 0; k < plist.length; k++) {
        result = result.concat(ternary(plist[i], plist[j], plist[k], allowed));
      }
    }
  }
  return plist.concat(result);
}

function bottomUpFaster(globalBnd, intOps, boolOps, vars, consts, inputoutputs){
  var plist = initializePlist(vars, consts);
  console.log(plist.map(p => p.toString()));
  var i = 0, j;
  while(i < globalBnd) {
    plist = growFaster(plist, intOps, boolOps);
    console.log(plist.map(p => p.toString()));
    plist = elimEquivalents(plist, inputoutputs);
    console.log(plist.map(p => p.toString()));

    for(j = 0; j < plist.length; j++) {
      if(isCorrect(plist[j], inputoutputs)){
        return plist[j];
      }
    }
    i++;
  }
  return "FAIL";
}


function run1a1(){
	
	var rv = bottomUp(3, [VR, NUM, PLUS, TIMES, ITE], [AND, NOT, LT, FALSE], ["x", "y"], [4, 5], [
    {x:5,y:10, _out:5},
    {x:8,y:3, _out:3}
  ]);
	writeToConsole("RESULT: " + rv.toString());
}


function run1a2(){
	
	var rv = bottomUp(3, [VR, NUM, PLUS, TIMES, ITE], [AND, NOT, LT, FALSE], ["x", "y"], [-1, 5], [
		{x:10, y:7, _out:17},
		{x:4, y:7, _out:-7},
		{x:10, y:3, _out:13},
		{x:1, y:-7, _out:-6},
		{x:1, y:8, _out:-8}		
	]);
	writeToConsole("RESULT: " + rv.toString());
	
}

function run1a3() {
  var rv = bottomUp(3, [VR, NUM, PLUS, TIMES, ITE], [AND, NOT, LT, FALSE], ["x"], [], [
    {x:10, _out:100},
    {x:4, _out:16},
    {x:5, _out:25},
    {x:20, _out: 400}
  ]);
  writeToConsole("RESULT: " + rv.toString());
}


function run1b(){
	
	var rv = bottomUpFaster(3, [VR, NUM, PLUS, TIMES, ITE], [AND, NOT, LT, FALSE], ["x", "y"], [-1, 5], [
		{x:10, y:7, _out:17},
		{x:4, y:7, _out:-7},
		{x:10, y:3, _out:13},
		{x:1, y:-7, _out:-6},
		{x:1, y:8, _out:-8}		
		]);
	writeToConsole("RESULT: " + rv.toString());
}


const f1 = (x, y) => y - 2*x;
const term1 = (x, y) => 2*x + y;
const f2 = (x, y) => y - x*x;
const term2 = (x, y) => x*x + y;
const f3 = (x, y) => y - 3*x;
const term3 = (x, y) => 3*x + y;

const apply = (f, p) => f(p[0], p[1]);

// Finds all elements shared between two arrays.
// If a is of length m and b is of length n then
// it works in O(n*m) time.
// Since we ever only call it on arrays of size up to 3,
// every time we call it takes constant time.
function intersect(a, b) {
  var setA = new Set(a);
  var setB = new Set(b);
  var intersection = new Set([...setA].filter(x => setB.has(x)));
  return Array.from(intersection);
}

// findNextBound iterates over array from index startingAt,
// discovering constants that can fill the holes in terms 1, 2
// and 3 until it either stops at the end of the array or
// gets to an element of the array with no constant
// shared with the previous elements. At that point, 
// it returns an object containing the index of the first 
// element with no shared constants, the shared constant between
// all the elements from startingAt to the one that shared none
// (or a random one of the constants if there's more than one)
// and the element that broke the chain of shared constants.
//
// We call this function a total of four times and each time
// takes no more than O(n) time (where n is the length of the array)
// so in total all our calls add up to linear time.
function findNextBound(array, startingAt) {
  var cs = [f1, f2, f3].map(f => apply(f, array[startingAt]));
  var i;
  var t, b;
  var inter, css;

  if(startingAt == array.length -1)
    return {i: startingAt, t: randElem(cs), b: array[startingAt][0]};
  
  // iterate from the element after startingAt
  for(i = startingAt+1; i < array.length; i++) {
    // find constants that would be needed for current
    // input to get current output with all three possible expressions
    css = [f1, f2, f3].map(f => apply(f, array[i]))
    // find intersection between these constants and previous
    // elements'
    inter = intersect(cs, css);

    // if we're at the end of the array, 
    // set potential constant to random element from shared
    // constants we've found so far, and set bound to last element of array.
    // stop iteration.
    if(i == array.length-1) {
      if(inter.length > 0){
        cs = inter;
      }
      t = randElem(cs);
      b = array[i][0];
      break;
    }
  
    // if current element shares no constants with previous elements,
    // set potential constant to random element from
    // shared constants discovered so far, set bound to 
    // current element and stop iteration.
    if(inter.length == 0) {
      t = randElem(cs);
      b = array[i][0];
      break;
    }

    // if we haven't broken out of iteration yet, set constants found so 
    // far (cs) to last computed intersection.
    cs = inter;
  }

  // return the index of the element that broke the chain of
  // shared constants, (one of) the shared constants we found
  // and value of element that broke the chain.
  return {i: i, t: t, b: b};
}


function structured(inputoutputs){

  // we call findNextBound four times to find the up to
  // four points where the program should branch, and what
  // constant those branches have to use to match the outputs
  // with the inputs. Each call takes up to O(n) time.
  var res1 = findNextBound(inputoutputs, 0);
  var res2 = findNextBound(inputoutputs, res1.i);
  var res3 = findNextBound(inputoutputs, res2.i);
  var res4 = findNextBound(inputoutputs, res3.i);
  
  // next we find what term corresponds to each branch, by 
  // running each term with an element of the branch and the 
  // constant we found earlier and making sure it matches its
  // output. Each call takes constant time.
  var t1 = [term1, term2, term3].
            filter(f => f(inputoutputs[res1.i-1][0], res1.t) == inputoutputs[res1.i-1][1])[0];
  var t2 = [term1, term2, term3].
            filter(f => f(inputoutputs[res2.i-1][0], res2.t) == inputoutputs[res2.i-1][1])[0];
  var t3 = [term1, term2, term3].
            filter(f => f(inputoutputs[res3.i-1][0], res3.t) == inputoutputs[res3.i-1][1])[0];
  var t4 = [term1, term2, term3].
            filter(f => f(inputoutputs[res4.i-1][0], res4.t) == inputoutputs[res4.i-1][1])[0];

  // next we create the expressions for each branch.
  // it's ugly, but I couldn't find a way to make it prettier.
  // constant time.
  var expr1 = (t1 == term1) ? 
    plus(times(num(2), vr("x")), num(res1.t)) :
    (t1 == term2) ?
    plus(times(vr("x"), vr("x")), num(res1.t)) :
    (t1 == term3) ?
    plus(times(num(3), vr("x")), num(res1.t)) :
    undefined;
  var expr2 = (t2 == term1) ? 
    plus(times(num(2), vr("x")), num(res2.t)) :
    (t2 == term2) ?
    plus(times(vr("x"), vr("x")), num(res2.t)) :
    (t2 == term3) ?
    plus(times(num(3), vr("x")), num(res2.t)) :
    undefined;
  var expr3 = (t3 == term1) ? 
    plus(times(num(2), vr("x")), num(res3.t)) :
    (t3 == term2) ?
    plus(times(vr("x"), vr("x")), num(res3.t)) :
    (t3 == term3) ?
    plus(times(num(3), vr("x")), num(res3.t)) :
    undefined;
  var expr4 = (t4 == term1) ? 
    plus(times(num(2), vr("x")), num(res4.t)) :
    (t4 == term2) ?
    plus(times(vr("x"), vr("x")), num(res4.t)) :
    (t4 == term3) ?
    plus(times(num(3), vr("x")), num(res4.t)) :
    expr3;

  // next we create the conditions for the
  // branches with the bounds we found earlier.
  // constant time.
  var comp1 = lt(vr("x"), num(res1.b));
  var comp2 = lt(vr("x"), num(res2.b));
  var comp3 = lt(vr("x"), num(res3.b));

  // finally we create the complete program 
  // with the conditions and expressions.
  // constant time.
  var prog = ite(
    comp1,
    expr1,
    ite(
      comp2,
      expr2,
      ite(
        comp3,
        expr3,
        expr4
      )
    )
  )
  
	return prog;
}


function run2() {
    var inpt = JSON.parse(document.getElementById("input2").value);
    /// !!!!! CHEATING !!!!!!!!!!!!
    var prog = structured(inpt.sort((a,b) => a[0] - b[0]));
    /// !!!!! We pass the input/ouput list SORTED in INCREASING ORDER by INPUT
    /// !!!!! Couldn't figure out how to do it without doing this.
    /// !!!!! I'm so sorry.

    writeToConsole(prog.toString());
    var works = !inpt.map(t => interpret(prog, {x: t[0]}) == t[1]).includes(false);
    if(works)
      writeToConsole("Program is correct for given inputs.");
    else
      writeToConsole("Program is incorrect.")
}


function genData() {
    //If you write a block of code in program1 that writes its output to a variable out,
    //and reads from variable x, this function will feed random inputs to that block of code
    //and write the input/output pairs to input2.
    program = document.getElementById("program1").value
    function gd(x) {
        var out;
        eval(program);
        return out;
    }
    textToIn = document.getElementById("input2");
    textToIn.value = "[";
    var lim = 50
    for(i=0; i<lim; ++i){
        if(i!=0){ textToIn.textContent += ", "; }
        var inpt = randInt(0, 100);
        textToIn.value += "[" + inpt + ", " + gd(inpt) + "]";
        if(i<lim-1)
          textToIn.value += ",";
    }
    textToIn.value += "]";
}
