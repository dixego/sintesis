# Package

version       = "0.1.0"
author        = "Diego Carrillo"
description   = "Project for Synthesis Course - Generating image filters"
license       = "GPLv3"
srcDir        = "src"
bin           = @["proyecto", "generator"]

# Dependencies

requires "nim >= 0.19.0", "stbimage", "patty"
