{.experimental: "implicitDeref".}
import patty
import strformat

# Warning: This file contains a high density of crimes against humanity.

proc `~`*[A](a: A): ref A =
  new(result)
  result[] = a

type
  Index* = range[1..3]
  Pixel* = tuple[r,g,b: int]
  OpKind* = enum Add, Sub, Mul, Div, And

variantp Numeric:
  Val(v: int)
  Proj(pos: Index)
  BinOp(op: OpKind, l: ref Numeric, r: ref Numeric)

# It is a well known fact that custom operators make programs easier to
# understand.
proc `+`*(l, r: Numeric): Numeric =       BinOp(Add, ~l, ~r)
proc `+`*(l: Numeric, r: int): Numeric =  BinOp(Add, ~l, ~Val(r))
proc `+`*(l: int, r: Numeric): Numeric =  BinOp(Add, ~Val(l), ~r)

proc `-`*(l, r: Numeric): Numeric =        BinOp(Sub, ~l, ~r)
proc `-`*(l: Numeric, r: int): Numeric =  BinOp(Sub, ~l, ~Val(r))
proc `-`*(l: int, r: Numeric): Numeric =  BinOp(Sub, ~Val(l), ~r)

proc `*`*(l, r: Numeric): Numeric =       BinOp(Mul, ~l, ~r)
proc `*`*(l: Numeric, r: int): Numeric =  BinOp(Mul, ~l, ~Val(r))
proc `*`*(l: int, r: Numeric): Numeric =  BinOp(Mul, ~Val(l), ~r)

proc `/`*(l, r: Numeric): Numeric =       BinOp(Div, ~l, ~r)
proc `/`*(l: Numeric, r: int): Numeric =  BinOp(Div, ~l, ~Val(r))
proc `/`*(l: int, r: Numeric): Numeric =  BinOp(Div, ~Val(l), ~r)

proc `&`*(l, r: Numeric): Numeric =       BinOp(And, ~l, ~r)
proc `&`*(l: Numeric, r: int): Numeric =  BinOp(And, ~l, ~Val(r))
proc `&`*(l: int, r: Numeric): Numeric =  BinOp(And, ~Val(l), ~r)

proc `==`(l, r: Numeric): bool =
  case l.kind
  of NumericKind.Val:
    r.kind == NumericKind.Val and l.v == r.v
  of NumericKind.Proj:
    r.kind == NumericKind.Proj and l.pos == r.pos
  of NumericKind.BinOp:
    r.kind == NumericKind.BinOp and l.op == r.op and l.l == r.l and l.r == r.r
  else:
    false

proc `$`*(n: Numeric): string =
  match n:
    Val(v): &"Val({v})"
    Proj(p): "π" & $p
    BinOp(op, l, r):
      case op
      of Add: $l & " + " & $r
      of Sub: $l & " - " & $r
      of Mul: $l & " * " & $r
      of Div: $l & " / " & $r
      of And: $l & " & " & $r

let pi1* = Proj(1)
let pi2* = Proj(2)
let pi3* = Proj(3)
let π1* = Proj(1)
let π2* = Proj(2)
let π3* = Proj(3)

proc eval*(op: OpKind, l, r: int): int =
  case op:
  of Add: l + r
  of Sub: l - r
  of Mul: l * r
  of Div: l div 3
  of And: l and r

proc eval*(num: Numeric, p: Pixel = (0,0,0)): int =
  result = match num:
    Val(n): n
    BinOp(op, l, r): op.eval(l.eval(p), r.eval(p))
    Proj(pos):
      case pos
        of 1: p.r
        of 2: p.g
        of 3: p.b

#[ Remnants from a different era...
type CompKind* = enum Le, Eq, Gt
variantp Boolean:
  Comp(op: CompKind, l: ref Numeric, r: ref Numeric)

proc `<=`*(l, r: Numeric): Boolean =       Comp(Le, ~l, ~r)
proc `<=`*(l: int, r: Numeric): Boolean =  Comp(Le, ~Val(l), ~r)
proc `<=`*(l: Numeric, r: int): Boolean =  Comp(Le, ~l, ~Val(r))

proc `eq`*(l, r: Numeric): Boolean =       Comp(Eq, ~l, ~r)
proc `eq`*(l: int, r: Numeric): Boolean =  Comp(Eq, ~Val(l), ~r)
proc `eq`*(l: Numeric, r: int): Boolean =  Comp(Eq, ~l, ~Val(r))

proc `>`*(l, r: Numeric): Boolean =        Comp(Gt, ~l, ~r)
proc `>`*(l: int, r: Numeric): Boolean =   Comp(Gt, ~Val(l), ~r)
proc `>`*(l: Numeric, r: int): Boolean =   Comp(Gt, ~l, ~Val(r))

proc `$`*(b: Boolean): string =
  match b:
    Comp(op, l, r):
      case op
      of Le: $l & " <= " & $r
      of Eq: $l & " == " & $r
      of Gt: $l & " > "  & $r


proc eval*(b: Boolean, p:Pixel): bool =
  match b:
    Comp(op, l, r):
      case op
      of Le: l.eval(p) <= r.eval(p)
      of Eq: l.eval(p) == r.eval(p)
      of Gt: l.eval(p) > r.eval(p)
]#
type 
  Branch* = tuple[bound: int, body: ref Program]
  ProgramKind* {.pure.} = enum Pix, Case
  Program* = object
    case kind: ProgramKind
    of Pix:
      r: ref Numeric
      g: ref Numeric
      b: ref Numeric
    of Case:
      value: ref Numeric
      branches: seq[Branch]
      default: ref Program



proc Pix*(r, g, b: ref Numeric): Program =
  Program(kind: ProgramKind.Pix, r: r, g: g, b: b)
proc Case*(value: Numeric, default: Program, branches: varargs[Branch]): Program =
  Program(kind: ProgramKind.Case, value: ~value, default: ~default, branches: @branches)
proc Case*(value: Numeric, default: Program, branches: seq[Branch]): Program =
  Program(kind: ProgramKind.Case, value: ~value, default: ~default, branches: branches)

proc branch*(b: int, body: Program): Branch = (bound: b, body: ~body)

proc pix*(r, g, b: Numeric): Program = Pix(~r, ~g, ~b)
proc replicate*(e: Numeric): Program = Pix(~e, ~e, ~e)
proc replicate*(e: int): Program = replicate(~Val(e))


proc `$`*(b: Branch): string
proc `$`*(p: Program): string =
  match p:
    Pix(r, g, b):
      result = "pix(" & $r & ", " & $g & ", " & $b & ")"
    Case(value, branches, default):
      result= &"Case({value}, {default}, {branches})"
proc `$`*(b: Branch): string =
  result = &"branch({b.bound}," & $b.body & ")"

proc clamp*(n: int): int =
  if n > 255:
    return 255
  if n < 0:
    return 0
  return n

proc clamp*(p: Pixel): Pixel =
  (p.r.clamp(), p.g.clamp(), p.b.clamp())

proc eval*(prog: Program, p: Pixel): Pixel =
  match prog:
    Pix(r, g, b):
      result = (r: r.eval(p), g: g.eval(p), b: b.eval(p))
    Case(value, branches, default):
      let v = value.eval(p)
      var branched = false
      for b in branches:
        if v < b.bound:
          result = b.body.eval(p)
          branched = true
          break
      if not branched:
        result = default.eval(p)
  result = result.clamp()
