import strformat, sugar, random, sets, sequtils, sets, hashes, algorithm
import pixlang, simple_image, util

var numTerminal = @[pi1, pi2, pi3, Val(0), Val(128), Val(255), Val(3), Val(2)]
let brightness = (pi1 + pi2 + pi3) / 3
type 
  Synthesizer* = ref object
    inputImg: StbImage
    outputImg: StbImage
    rng: Rand
    samples: seq[(Pixel, Pixel)]
    allowedOps: seq[OpKind]
    maxBranches: int
    maxNumericDepth: int
    constants: seq[Numeric]

proc initSynthesizer*(): Synthesizer =
  new(result)

proc input*(s: Synthesizer, filename: string): Synthesizer =
  s.inputImg = filename.loadFromFile()
  return s
proc output*(s: Synthesizer, filename: string): Synthesizer =
  s.outputImg = filename.loadFromFile()
  return s
proc rand*(s: Synthesizer, seed: int64): Synthesizer =
  s.rng = initRand(seed)
  return s
proc samples*(s: Synthesizer, size: int): Synthesizer =
  var samples = newSeq[(Pixel, Pixel)]()
  for _ in 0..<size:
    let x = s.rng.rand(s.inputImg.width() - 1)
    let y = s.rng.rand(s.inputImg.height() - 1)
    samples.add((s.inputImg[x,y].toPixel(), s.outputImg[x,y].toPixel()))
  s.samples = samples
  return s
proc allowed*(s: Synthesizer, ops: varargs[OpKind]): Synthesizer =
  s.allowedOps = @ops
  return s
proc maxBranches*(s: Synthesizer, bound: int): Synthesizer =
  s.maxBranches= bound
  return s
proc maxNumericDepth*(s: Synthesizer, bound: int): Synthesizer =
  s.maxNumericDepth = bound
  return s
proc constants*(s: Synthesizer, consts: varargs[int]): Synthesizer =
  s.constants = consts.mapIt(Val(it))
  return s
proc `$`*(s: Synthesizer): string =
  result = &"({s.samples.len} samples, {s.allowedOps}, {s.maxBranches}, {s.maxNumericDepth})"


proc grow(s: Synthesizer, nlist: seq[Numeric]): seq[Numeric] =
  result.add(nlist)
  for n in nlist:
    for m in nlist:
      if n.kind == NumericKind.Val and m.kind == NumericKind.Val:
        continue
      if Add in s.allowedOps:
        result.add(n + m)
      if Mul in s.allowedOps:
        result.add(n * m)
      if Sub in s.allowedOps:
        result.add(n - m)
      if Div in s.allowedOps:
        result.add(n / m)
      if And in s.allowedOps:
        result.add(n & m)

proc generatePrograms(s: Synthesizer, plist: seq[Program],
                      branches: int, bounds: seq[int]): seq[Program] =
  let bnds = bounds.sortedSubsets(branches-1)
  for b in bnds:
    var brnchs = newSeq[Branch]()
    brnchs.add(lc[branch(bb, p) | (bb <- b, p <- plist), Branch])
    let brs = lc[Case(brightness, def, brs) | (def <- plist, brs <- brnchs), Program]
    result.add(brs)
  #echo result.len()
  #echo "we did it?"

proc isCorrect(p: Program, s: seq[(Pixel, Pixel)]): bool =
  for sample in s:
    if p.eval(sample[0]) != sample[1]:
      return false
  return true

proc equal(p1, p2: Program, samples: seq[(Pixel, Pixel)]): bool =
  for sample in samples:
    if p1.eval(sample[0]) != p2.eval(sample[0]):
      return false
  return true

proc prune(s: Synthesizer, plist: var seq[Program]): seq[Program] =
  var i = 0
  var j = 1
  var dead = newSeq[int]()
  while i < plist.len():
    while j < plist.len():
      if equal(plist[i], plist[j], s.samples):
        dead.add(j)
      j += 1
    i += 1
    j = i + 1

  i = 0
  while i < plist.len():
    if i in dead:
      i += 1
      continue
    result.add(plist[i])
    i += 1

proc isVal(n: NumericKind): bool =
  case n
  of NumericKind.Val:
    true
  else:
    false

proc constants(n: seq[Numeric]): seq[Numeric] =
  n.filter(n => n.kind.isVal())

proc synthesize*(synth: Synthesizer): Program =
  numTerminal.add(synth.constants)
  var pList = lc[pix(a,b,c) | (a <- numTerminal, b <- numTerminal, c <- numTerminal), Program]
  var constants = numTerminal.constants().mapIt(it.v)
  constants.sort(system.cmp[int])

  #echo constants
  #echo pList
  for _ in 0..synth.maxNumericDepth:
    for p in pList:
      if p.isCorrect(synth.samples):
        return p

    for b in 2..synth.maxBranches:
      let p = synth.generatePrograms(pList, b, constants)
      for prog in p:
        if prog.isCorrect(synth.samples):
          return prog
    numTerminal = synth.grow(numTerminal)
    pList = lc[pix(a,b,c) | (a <- numTerminal, b <- numTerminal, c <- numTerminal), Program]

  echo "no se encontró el programa buscado"
  return pList[0]

