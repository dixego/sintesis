import pixlang
import parseopt, random, sequtils, algorithm, sugar
import os, ospaths, strutils, strformat, util, simple_image as image

type Generator = ref object
  numericExprs: seq[Numeric]
  rng: Rand
  maxBranches: int
  maxNumericDepth: int
  allowedOps: seq[OpKind]
  constants: seq[int]

proc `$`*(g: Generator): string =
  &"""Generator(
    numericExprs: {g.numericExprs}
    maxBranches: {g.maxNumericDepth}
    maxNumericDepth: {g.maxNumericDepth}
    allowedOps: {g.allowedOps}
    constants: {g.constants}
  )"""

proc initGenerator(): Generator =
  new(result)

proc genNumeric(g: Generator, maxDepth: int): Numeric =
  let depth = g.rng.rand(0..maxDepth)
  if depth == 0:
    result = g.rng.rand(g.numericExprs)
  else:
    result = BinOp(g.rng.rand(g.allowedOps), ~g.genNumeric(maxDepth-1), ~g.genNumeric(maxDepth-1))

proc genConstant(g: Generator): int =
  g.rng.rand(g.constants)

proc genPix(g: Generator): Program =
  let r = g.genNumeric(g.maxNumericDepth)
  let v = g.genNumeric(g.maxNumericDepth)
  let b = g.genNumeric(g.maxNumericDepth)
  result = pix(r, v, b)

proc genBranch(g: Generator): Branch =
  let bound = g.genConstant()
  let body = g.genPix()
  result = branch(bound, body)

proc makeBranches(g: Generator, nBranches: int, progs: seq[Program]): seq[Branch] =
  var consts #[lol]#: seq[int]
  if nBranches == g.constants.len():
    consts = g.constants.sorted(system.cmp[int])
    return zip(g.constants.sorted(system.cmp[int]), progs).mapIt(branch(it[0], it[1]))
  else:
    var consts = newSeq[int]()
    while consts.len() < nBranches:
      let c = g.genConstant()
      if c in consts:
        continue
      consts.add(c)
  result = zip(g.constants.sorted(system.cmp[int]), progs).mapIt(branch(it[0], it[1]))

proc genPrograms(g: Generator, nProgs: int): seq[Program] =
  var caseEnabled = g.maxBranches > 1

  if not caseEnabled:
    result = lc[g.genPix() | (_ <- 1..nProgs), Program]
  else:
    while result.len() < nProgs:
      let choice = g.rng.rand([true, false])
      if choice:
        let branches = g.rng.rand(1..g.maxBranches - 1)
        result.add(
          Case(
            g.genNumeric(g.maxNumericDepth),
            g.genPrograms(1)[0],
            g.makeBranches(branches, g.genPrograms(branches))))
      else:
        result.add(g.genPix())

when isMainModule:
  echo "hewwo"

  let infile = paramStr(1).string
  let n = paramStr(2).string.parseInt()
  let seed = paramStr(3).string.parseInt()

  let g = initGenerator()
  g.numericExprs = @[pi1, pi2, pi3, Val(0), Val(128), Val(255), Val(3), (pi1 + pi2 + pi3)/3]
  g.rng = initRand(seed)
  g.maxBranches = 2
  g.maxNumericDepth = 3
  g.allowedOps = @[Add, Mul, Sub, Div, And]
  g.constants = @[64, 128, 192]
  
  let name = infile.splitFile().name
  let progs = g.genPrograms(n)

  for i in 0..progs.high():
    let res = progs[i].applyProgram(infile)
    res.writePng(&"{name}{i}.png")
    echo &"Filtro para el archivo {name}{i}.png: "
    echo progs[i]

  echo g
  echo &"Semilla: {seed}"
