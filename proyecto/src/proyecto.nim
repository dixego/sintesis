import strformat, sugar, random
import simple_image as image
import pixlang, util, synth

var img = loadFromFile("holi.jpg")
let (w,h) = (img.width, img.height)
randomize()

let brightness = (pi1+pi2+pi3)/3
#let program = Case(brightness, pix(pi1, Val(0), Val(0)), branch(128, pix(Val(0), pi2, Val(0))))
#let contrast = Case(brightness, replicate(0), branch(128, replicate(255)))
let program = Case(brightness, replicate(255), branch(128, replicate(0)))
let im2 = program.applyProgram(img)
im2.writePng("holi.png")

let syn = initSynthesizer()
   .input("holi.jpg")
   .output("holi.png")
   .rand(999)
   .samples(100)
   .allowed(Add, Mul)
   .maxBranches(2)
   .maxNumericDepth(1)
   .constants(10)

echo syn
echo syn.synthesize()
