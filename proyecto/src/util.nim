import hashes, strformat, sugar
import pixlang
import simple_image

proc `$`*(p: Pixel): string =
  fmt"({p.r}, {p.g}, {p.b})"

proc `$`*(p: (Pixel, Pixel)): string =
  fmt"(in: {p[0]}, out: {p[1]})"

proc hash*(n: Numeric): Hash =
  hash($n)

proc toRgbaPixel*(p: Pixel): RgbaPixel =
  newRgba(p.r, p.g, p.b, 255)

proc toPixel*(p: RgbaPixel): Pixel =
  (p.r.int, p.g.int, p.b.int)

proc sortedSubsets*[T](list: seq[T], n: int): seq[seq[T]] =
  if n == 0:
    return @[]
  if n == 1:
    return lc[@[n] | (n <- list), seq[T]]
  if n == list.len() or n > list.len():
    return @[list]
  for i in 0..list.high():
    let rest = list[i+1..list.high()].sortedSubsets(n-1)
    let ad = lc[(@[list[i]] & l) | (l <- rest), seq[T]]
    result.add(ad)

proc subsets*[T](list: seq[T], n: int): seq[seq[T]] =
  if n == 0:
    return @[]
  if n == 1:
    return lc[@[n] | (n <- list), seq[T]]
  if n == list.len() or n > list.len():
    return @[list]
  for i in 0..list.high():
    let rest = list.subsets(n-1)
    let ad = lc[(@[list[i]] & l) | (l <- rest), seq[T]]
    result.add(ad)

proc applyProgram*(prog: Program, img: StbImage): StbImage =
  result = img.copy()
  for x in 0..<img.width:
    for y in 0..<img.height:
      let pix = img[x, y]
      result[x, y] = prog.eval(pix.toPixel()).toRgbaPixel()

proc applyProgram*(prog: Program, img: string): StbImage =
  result = prog.applyProgram(loadFromFile(img))
