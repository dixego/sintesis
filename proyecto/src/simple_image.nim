# simple_image
# Copyright Diego Carrillo
# Simple abstraction over stb_image to manipulate images as pixel matrices

import stb_image/read as stbr
import stb_image/write as stbw
import strformat

# Types
# -----------------------------------------------------------------------------

type
  StbImage* = ref object ## image as represented by a 2D RGBA pixel matrix.
    width, height, channels: int
    pixelData: seq[seq[RgbaPixel]]
  RgbaPixelObj = tuple[r,g,b,a: byte]
  RgbaPixel* = ref RgbaPixelObj ## pixel as represented by an RGBA tuple \
                                    ## of bytes.

# -----------------------------------------------------------------------------


# Procs
# -----------------------------------------------------------------------------

proc newRgba*(rr, gg, bb, aa: byte): RgbaPixel =
  ## Convenience template for creating `RgbaPixel`'s from `byte`'s.
  new(result)
  result.r = rr
  result.g = gg
  result.b = bb
  result.a = aa

proc newRgba*(rr, gg, bb, aa: int): RgbaPixel =
  ## Similar to the `byte`s version but casts `int`'s to `byte`'s automatically.
  new(result)
  result.r = rr.byte
  result.g = gg.byte
  result.b = bb.byte
  result.a = aa.byte

proc `$`*(p: RgbaPixel): string =
  &"(R{p.r}, G{p.g}, B{p.b}, A{p.a})"

proc width*(image: StbImage): int =
  ## Return `image`'s width
  image.width

proc height*(image: StbImage): int =
  ## Return `image`'s height
  image.height

proc channels*(image: StbImage): int =
  ## Return `image`'s number of channels 
  image.channels

proc `[]`*(image: StbImage, x, y: int): RgbaPixel =
  ## Return the pixel at `image[x,y]`, where x is the row and y is the column
  image.pixelData[y][x]

proc `[]=`*(image: var StbImage, x, y: int, pix: RgbaPixel) =
  image.pixelData[y][x] = pix

proc toPixelData(bytes: seq[byte], w, h, ch: int): seq[seq[RgbaPixel]] =
  ## process a `byte` sequence representing a `w`x`h` image with `ch` color
  ## channels, returning a (more or less) equivalent `RgbaPixel` matrix
  ## representation. 
  ## If the image is one channel, it replicates the greyscale color along
  ## the rgb channels and sets the alpha channel to 255.
  ## If the image is two channels, it does the same but stores the alpha
  ## channel in its corresponding place
  ## If the image is three channels, it stores the color data in the rgb
  ## channels and sets the alpha channel to 255.
  ## If the image is four channels, it maps them to their corresponding
  ## places in the `RgbaPixel`.
  result = @[]
  var
    r, g, b, a: byte
    row: seq[RgbaPixel]

  for i in countup(0, bytes.len - 1, w * ch):
    row = @[]
    for j in countup(0, (w*ch) - 1, ch):
      (r,g,b,a) = case ch:
        of 1: (bytes[i+j], bytes[i+j], bytes[i+j], 255.byte)
        of 2: (bytes[i+j], bytes[i+j], bytes[i+j], bytes[i+j+1])
        of 3: (bytes[i+j], bytes[i+j+1], bytes[i+j+2], 255.byte)
        of 4: (bytes[i+j], bytes[i+j+1], bytes[i+j+2], bytes[i+j+3])
        else: (0.byte, 0.byte, 0.byte, 0.byte)
      row.add(newRgba(r, g, b, a))
    result.add(row)

proc toByteSeq(pixels: seq[seq[RgbaPixel]], ch: int): seq[byte] =
  ## Inverse operation of `toPixelData`; that is, if `bytes` is a
  ## `seq[byte]` representation of an `w x h` image with `ch` channels, then
  ## `bytes.toPixelData(w,h,ch).toByteSeq(ch) == bytes`.
  result = @[]
  var
    r,g,b,a: byte
  for row in pixels:
    for p in row:
      (r,g,b,a) = p
      case ch:
        of 1:
          result.add(r)
        of 2:
          result.add(r)
          result.add(a)
        of 3:
          result.add(r)
          result.add(g)
          result.add(b)
        of 4:
          result.add(r)
          result.add(g)
          result.add(b)
          result.add(a)
        else: discard

proc toByteSeq*(image: StbImage): seq[byte] =
  ## Returns a sequence of bytes that represent the `image`'s pixel information
  image.pixelData.toByteSeq(image.channels)

proc loadFromFile*(filename: string): StbImage =
  ## Loads an image from file `filename`
  var
    w,h,ch: int
    data: seq[byte]
  data = stbr.load(filename, w, h, ch, stbr.Default)
  new(result)
  result.width = w
  result.height = h
  result.channels = ch
  result.pixelData = data.toPixelData(w, h, ch)

proc loadFromMemory*(buffer: seq[byte]): StbImage =
  ## Loads an image from in-memory buffer `buffer`
  var
    w,h,ch: int
    data: seq[byte]
  data = stbr.loadFromMemory(buffer, w, h, ch, stbr.Default)
  new(result)
  result.width = w
  result.height = h
  result.channels = ch
  result.pixelData = data.toPixelData(w, h, ch)

proc loadFromRawData*(buffer: seq[byte], w, h, ch: int): StbImage =
  new(result)
  result.width = w
  result.height = h
  result.channels = ch
  result.pixelData = buffer.toPixelData(w, h, ch)


proc copy*(origin: StbImage): StbImage =
  new(result)
  var
    rows = newSeq[seq[RgbaPixel]]()
  
  for row in origin.pixelData:
    var new_row = newSeq[RgbaPixel]()
    for p in row:
      new_row.add(newRgba(p.r, p.g, p.b, p.a))
    rows.add(new_row)

  result.pixelData = rows
  result.width = origin.width
  result.height = origin.height
  result.channels = origin.channels

proc writePng*(img: StbImage, filename: string) =
  stbw.writePNG(filename, img.width, img.height, img.channels, img.toByteSeq())

proc writeJpg*(img: StbImage, filename: string) =
  stbw.writeJPG(filename, img.width, img.height, img.channels, img.toByteSeq(), 100)

proc writeBmp(img: StbImage, filename: string) =
  stbw.writeBMP(filename, img.width, img.height, img.channels, img.toByteSeq())

iterator pixels*(img: StbImage): RgbaPixel =
  for x in 0..<(img.width):
    for y in 0..<(img.height):
      yield img[x,y]
