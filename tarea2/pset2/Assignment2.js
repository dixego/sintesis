// JavaScript source code



////////////////
//
// Problem 2
//
// Solution by 
// - Carrillo Verduzco, Diego          - dixego@ciencias.unam.mx
// - Galicia Mendoza, Fernando Abigail - fernandogamen@ciencias.unam.mx
//
///////////////

var NUM = "NUM";
var FALSE = "FALSE";
var VR = "VAR";
var PLUS = "PLUS";
var TIMES = "TIMES";
var LT = "LT";
var AND = "AND";
var NOT = "NOT";

var SEQ = "SEQ";
var IFTE = "IFSTMT";
var WHLE = "WHILESTMT";
var ASSGN = "ASSGN";
var SKIP = "SKIP";
var ASSUME = "ASSUME";
var ASSERT = "ASSERT";

var INVAR = "INVAR";

var OR = "OR";
var EQ = "EQ";
var LE = "LE";

function str(obj) { return JSON.stringify(obj); }

//Constructor definitions for the different AST nodes.

function flse() {
    return { type: FALSE, toString: function () { return "false"; } };
}

function vr(name) {
    return { type: VR, name: name, toString: function () { return this.name; } };
}
function num(n) {
    return { type: NUM, val: n, toString: function () { return this.val; } };
}
function plus(x, y) {
    return { type: PLUS, left: x, right: y, toString: function () { return "(" + this.left.toString() + "+" + this.right.toString() + ")"; } };
}
function times(x, y) {
    return { type: TIMES, left: x, right: y, toString: function () { return "(" + this.left.toString() + "*" + this.right.toString() + ")"; } };
}
function lt(x, y) {
    return { type: LT, left: x, right: y, toString: function () { return "(" + this.left.toString() + "<" + this.right.toString() + ")"; } };
}
function and(x, y) {
    return { type: AND, left: x, right: y, toString: function () { return "(" + this.left.toString() + "&&" + this.right.toString() + ")"; } };
}

function or(x, y) {
    return { type: OR, left: x, right: y, toString: function () { return "(" + this.left.toString() + "||" + this.right.toString() + ")"; } };
}

function eq(x, y) {
    return { type: EQ, left: x, right: y, toString: function () { return "(" + this.left.toString() + "==" + this.right.toString() + ")"; } };
}

function le(x, y) {
    return { type: LE, left: x, right: y, toString: function () { return "(" + this.left.toString() + "<=" + this.right.toString() + ")"; } };
}

function not(x) {
    return { type: NOT, left: x, toString: function () { return "(!" + this.left.toString() + ")"; } };
}


function seq(s1, s2) {
    return { type: SEQ, fst: s1, snd: s2, toString: function () { return "" + this.fst.toString() + "\n" + this.snd.toString(); } };
}


function assume(e) {
    return { type: ASSUME, exp: e, toString: function () { return "assume " + this.exp.toString() + ";"; } };
}

function assert(e) {
    return { type: ASSERT, exp: e, toString: function () { return "assert " + this.exp.toString() + ";"; } };
}

function assgn(v, val) {
    return { type: ASSGN, vr: v, val: val, toString: function () { return "" + this.vr + ":=" + this.val.toString() + ";"; } };
}

function ifte(c, t, f) {
    return { type: IFTE, cond: c, tcase: t, fcase: f, toString: function () { return "if(" + this.cond.toString() + "){\n" + this.tcase.toString() + '\n}else{\n' + this.fcase.toString() + '\n}'; } };
}

function whle(c, b) {
    return { type: WHLE, cond: c, body: b, toString: function () { return "while(" + this.cond.toString() + "){\n" + this.body.toString() + '\n}'; } };
}

function skip() {
    return { type: SKIP, toString: function () { return "/*skip*/"; } };
}

function invariant(nme, exprs) {
  return { type: INVAR, name: nme, exprs: exprs, toString: function() { 
    var vs = this.exprs.map(v => v.toString()).join(", ")
    return this.name + "("+ vs +")";
  }};
}

//some useful helpers:

/*function eq(x, y) {
    return and(not(lt(x, y)), not(lt(y, x)));
}*/

function tru() {
    return not(flse());
}

/*function le(x,y){
  return or(lt(x,y),eq(x,y));
}*/


function block(slist) {
    if (slist.length == 0) {
        return skip();
    }
    if (slist.length == 1) {
        return slist[0];
    } else {
        return seq(slist[0], block(slist.slice(1)));
    }
}

//The stuff you have to implement.
function subst(elem, pre, post) {
  switch(elem.type) {
    case VR: 
      if (elem.name === pre)
        return post;
      else 
        return elem;

    case NOT:
      var subsl = subst(elem.left, pre, post);
      return not(subsl);

    case PLUS:
    case TIMES:
    case AND:
    case OR:
    case LT:
    case EQ:
    case LE:
      var subsl = subst(elem.left, pre, post)
      var subsr = subst(elem.right, pre, post)
      return {type: elem.type, left: subsl, right: subsr,  toString: elem.toString};

    case INVAR:
      return invariant(elem.name, elem.exprs.map(v => subst(v, pre, post)));

    case SEQ:
      return seq(subst(elem.fst, pre, post), subst(elem.snd, pre, post));

    case IFTE:
      return ifte(
        subst(elem.cond, pre, post), 
        subst(elem.tcase, pre, post), 
        subst(elem.fcase, pre, post));

    case WHLE:
      return whle(subst(elem.cond, pre, post), subst(elem.body, pre, post));

    case ASSUME:
      return assume(subst(elem.exp, pre, post));

    default: return elem;
  }
}

function modifiedVars(prog) {
  var modified = new Set()

  switch(prog.type) {
    case SEQ:
      return union(modifiedVars(prog.fst), modifiedVars(prog.snd));
    case IFTE:
      return union(modifiedVars(prog.tcase), modifiedVars(prog.fcase));
    case WHLE:
      return modifiedVars(prog.body);
    case ASSGN:
      return new Set([prog.vr]);
    default: return modified;
  }
}

function getVars(prog) {
  var vars = new Set()

  switch(prog.type) {
    case ASSERT: 
    case ASSUME: 
      return union(vars, getVars(prog.exp));

    case ASSGN: 
      vars.add(prog.vr);
      return union(vars, getVars(prog.val));
    case IFTE:
      var condVars = getVars(prog.cond);
      var tVars = getVars(prog.tcase);
      var fVars = getVars(prog.fcase);
      return union(vars, union(condVars, union(tVars, fVars)));
    case WHLE:
      var condVars = getVars(prog.cond);
      var bodyVars = getVars(prog.body);
      return union(vars, union(condVars, bodyVars));

    case SEQ: 
      return union(getVars(prog.fst), getVars(prog.snd));
    
    case VR:
      vars.add(prog.name)
      return vars; break;
    
    case PLUS:
    case TIMES:
    case LT:
    case AND:
    case OR:
    case EQ:
    case LE:
      var varsLeft = getVars(prog.left)
      var varsRight = getVars(prog.right)
      return union(varsLeft, varsRight);

    case NOT:
      var varsLeft = getVars(prog.left)
      return varsLeft;

    case INVAR:
      return prog.exprs.map(e => getVars(e)).reduce((x,y) => union(x,y), vars);

    default:
      return vars;
  }
}

function union(setA, setB) {
  var u = new Set(setA)
  setB.forEach(b => u.add(b));
  return u;
}

function computeVcAux(prog, cond, invs, vars) {
  switch(prog.type) {
    case SKIP: 
      return cond;
    case ASSGN:
      return subst(cond, prog.vr, prog.val);
    case ASSERT:
      return seq(prog.exp, cond);
    case ASSUME:
      return ifte(prog.exp, cond, tru());
    case SEQ:
      var vcSnd = computeVcAux(prog.snd, cond, invs, union(vars, getVars(prog.fst)))
      return computeVcAux(prog.fst, vcSnd,invs, vars);
    case IFTE:
      var vrs = union(vars, getVars(prog.cond));
      return ifte(
        prog.cond, 
        computeVcAux(prog.tcase, cond, invs, vrs), 
        computeVcAux(prog.fcase, cond, invs, vrs));
    case WHLE:
      var condVars = getVars(prog.cond);
      var bodyVars = getVars(prog.body);
      var modified = [...modifiedVars(prog.body)];
      var invVars = [...union(vars, union(condVars, bodyVars))].map(v => vr(v));
      var inv = invariant("inv" + invs, invVars);
      var vcBody = computeVcAux(prog.body, inv, invs+1, union(vars, condVars));
      var inv_p = invariant(inv.name, invVars);
      var cnd = Object.assign({}, prog.cond);
      modified.forEach(v =>  {
        var modv = v + "__p"
        inv_p = subst(inv_p, v, vr(modv))
        cnd = subst(cnd, v, vr(modv))
        vcBody = subst(vcBody, v, vr(modv))
        cond = subst(cond, v, vr(modv))
      });
      return seq(inv, ifte(inv_p,
        ifte(cnd, vcBody, cond),
        tru()
      ));
  }
}

function computeVC(prog) {
    //Compute the verification condition for the program leaving some kind of place holder for loop invariants.
  // The input prog is an AST. The output is an AST representing the verification condition.
  return computeVcAux(prog, tru(), 0, new Set())
}

function isTrue(e) {
  return e.type === NOT && e.left.type === FALSE;
}

function assertify(vc) {
  switch(vc.type) {
    case SEQ:
      var ass1 = assertify(vc.fst)
      var ass2 = assertify(vc.snd)
      return seq(ass1, ass2); break;
    case IFTE:
      var ass1 = assertify(vc.tcase)
      var ass2 = assertify(vc.fcase)
      return ifte(vc.cond, ass1, ass2);
    default:
      return assert(vc);
  }
}

function getInvs(vc) {
  switch(vc.type) {
    case INVAR:
      var invs = {}
      invs[vc.name] = vc.exprs.length
      return invs; break;
    case SEQ:
      return Object.assign({}, getInvs(vc.fst), getInvs(vc.snd)); break;
    case ASSERT:
      return getInvs(vc.exp); break;
    case IFTE:
      return Object.assign({}, getInvs(vc.cond), getInvs(vc.tcase), getInvs(vc.fcase)); break;
    default: return {}; break;
  }
}

function generateVars(noVars) {
  var x = "x"
  var vars = []
  for(i = 0; i < noVars; i++) {
    vars.push(x + i)
  }
  return vars;
}

function generateInvs(invs) {
  var code = []
  for (var key in invs) {
    var vars = generateVars(invs[key])
    var str = "bit " + key + "("+ vars.map(v => "int " + v).join(', ') + ") {\n";
    str = str + "  return exprBool({"+ vars.join(', ') +"}, {PLUS});\n}";
    code.push(str);
  }
  return code;
}

function genSketch(vc) {
    //Write a pretty printer that generates a sketch from the verification condition.
    //You can write your generators as a separate library, but you may do better by creating generators specialized for your problem.
  //The input is the VC that was generated by computeVC. The output is a String 
  //representing the sketch that you can feed to the sketch solver to synthesize the invariants.
  //
  var ass = assertify(vc)
  var vars = getVars(ass);
  var invs = getInvs(ass);
  var invCode = generateInvs(invs);


  var sketch = "harness void vc(" + [...vars]
    .map(v => "int " + v)
    .join(", ") + ") {\n" + ass.toString() + "\n}"
  var lines = sketch.split(/\r?\n/)
  var sketchIndent = []
  var indent = 0;

  lines.forEach(l => {
    if(l.startsWith("}"))
      indent -= 1
    sketchIndent.push('  '.repeat(indent) + l)
    if(l.endsWith("{"))
      indent += 1
  })

  return invCode.join('\n\n') + '\n\n' +  sketchIndent.join('\n');
}

function prueba1(){
  var prog1 = seq(
    seq(
      seq(
        assume(le(num(0),vr("n"))),
        assume(le(num(0),vr("m"))),
        ),
      seq(
        seq(
          assgn("i",num(1)),
          assgn("r",vr("n"))
          ),
          whle(
            le(vr("i"),vr("m")),
            seq(
              assgn("r",plus(vr("r"),num(1))),
              assgn("i",plus(vr("i"),num(1))))
            )
          )
      ),
    //assert(and(lt(vr("m"),vr("i")),eq(vr("r"),plus(vr("n"),vr("m"))))));
    assert(eq(vr("r"), plus(vr("n"), vr("m")))));
  return prog1;
}

function test1(){
  var pr1 = prueba1();
  clearConsole();
  writeToConsole("Test 1:");
  writeToConsole(pr1.toString());
  writeToConsole("\nAST:");
  writeToConsole(computeVC(pr1).toString());
  writeToConsole("\nSketch:");
  writeToConsole(genSketch(computeVC(pr1)).toString());
}

function prueba2(){
  var prog2 = seq(
    seq(
      seq(
        assume(le(num(0),vr("n"))),
        assume(le(num(0),vr("m"))),
        ),
      seq(
        seq(
          assgn("i", num(0)),
          assgn("r", vr("n"))
          ),
          whle(
            le(vr("i"),vr("m")),
            seq(
              assgn("r", plus(vr("r"),vr("r"))),
              assgn("i", plus(vr("i"),num(1))))
            )
          )
      ),
    assert(and(le(vr("i"),vr("m")),le(vr("r"),times(vr("n"),vr("m"))))));
  return prog2;
}

function test2(){
  var pr2 = prueba2();
  clearConsole();
  writeToConsole("Test2:");
  writeToConsole(pr2.toString());
  writeToConsole("\nAST:");
  writeToConsole(computeVC(pr2).toString());
  writeToConsole("\nSketch:");
  writeToConsole(genSketch(computeVC(pr2)).toString());
}

function prueba3() {
  var prog3 = block([
    assume(eq(vr("x"), vr("x_0"))),
    assume(eq(vr("y"), vr("y_0"))),
    assume(lt(vr("x"), vr("y"))),
    assgn("t", plus(vr("y"), times(num(-1), vr("x")))),
    whle(lt(num(0), vr("t")), block([
      assgn("x", plus(vr("x"), num(1))),
      assgn("y", plus(vr("y"), num(-1))),
      assgn("t", plus(vr("t"), num(-1)))
    ])),
    assert(and(eq(vr("x"), vr("y_0")), eq(vr("y"), vr("x_0"))))
  ])
  return prog3;
}

function test3(){
  var pr3 = prueba3();
  clearConsole();
  writeToConsole("Test3:");
  writeToConsole(pr3.toString());
  writeToConsole("\nAST:");
  writeToConsole(computeVC(pr3).toString());
  writeToConsole("\nSketch:");
  writeToConsole(genSketch(computeVC(pr3)).toString());
}

function prueba4() {
  var prog4 = block([
    assume(eq(vr("x"), vr("y"))),
    assgn("t", num(0)),
    whle(lt(vr("t"), num(10)), block([
      ifte(lt(vr("t"), num(7)), block([
        assgn("x", plus(vr("x"), num(-1))),
        assgn("y", plus(vr("y"), num(-1)))
      ]), skip()),
      assgn("t", plus(vr("t"), num(1)))
    ])),
    assert(eq(vr("x"), vr("y")))
  ])
  return prog4;
}

function test4(){
  var pr4 = prueba4();
  clearConsole();
  writeToConsole("Test4:");
  writeToConsole(pr4.toString());
  writeToConsole("\nAST:");
  writeToConsole(computeVC(pr4).toString());
  writeToConsole("\nSketch:");
  writeToConsole(genSketch(computeVC(pr4)).toString());
}

function prueba5() {
  var prog5 = block([
    assume(lt(vr("x"), vr("y"))),
    assume(eq(times(vr("x"), vr("t")), vr("y"))),
    assgn("i", num(0)),
    whle(lt(vr("i"), vr("t")), block([
      assgn("i", plus(vr("i"), num(1)))
    ])),
    assert(eq(times(vr("x"), vr("i")), vr("y")))

  ])
  return prog5;
}

function test5(){
  var pr5 = prueba5();
  clearConsole();
  writeToConsole("Test5:");
  writeToConsole(pr5.toString());
  writeToConsole("\nAST:");
  writeToConsole(computeVC(pr5).toString());
  writeToConsole("\nSketch:");
  writeToConsole(genSketch(computeVC(pr5)).toString());
}

function P2a() {
    var prog = eval(document.getElementById("p2input").value);
    clearConsole();
    writeToConsole("Just pretty printing for now");
    writeToConsole(computeVC(prog).toString());
}

function P2b() {
    var prog = eval(document.getElementById("p2input").value);
    clearConsole();
    writeToConsole("Just pretty printing for now");
    writeToConsole(genSketch(computeVC(prog)).toString());
}


//Some functions you may find useful:
function randInt(lb, ub) {
    var rf = Math.random();
    rf = rf * (ub - lb) + lb;
    return Math.floor(rf);
}

function randElem(from) {
    return from[randInt(0, from.length)];
}

function writeToConsole(text) {
    var csl = document.getElementById("console");
    if (typeof text == "string") {
        csl.textContent += text + "\n";
    } else {
        csl.textContent += text.toString() + "\n";
    }
}

function clearConsole() {
    var csl = document.getElementById("console");
    csl.textContent = "";
}
