// Assignment 3
// Diego Carrillo Ferduzco
// Fernando Mendoza Galicia
// Code is basically identical to Assignment1.js, save for
// all commented parts.

var NUM = "NUM";
var FALSE = "FALSE";
var VR = "VAR";
var PLUS = "PLUS";
var TIMES = "TIMES";
var LT = "LT";
var AND = "AND";
var NOT = "NOT";
var ITE = "ITE";

var ALLOPS = [NUM, FALSE, VR, PLUS, TIMES, LT, AND, NOT, ITE];

function str(obj) { return JSON.stringify(obj); }

//Constructor definitions for the different AST nodes.

function flse() {
    return { type: FALSE, toString: function () { return "false"; } };
}

function vr(name) {
    return { type: VR, name: name, toString: function () { return this.name; } };
}
function num(n) {
    return { type: NUM, val: n, toString: function () { return this.val; } };
}
function plus(x, y) {
    return { type: PLUS, left: x, right: y, toString: function () { return "("+ this.left.toString() + "+" + this.right.toString()+")"; } };
}
function times(x, y) {
    return { type: TIMES, left: x, right: y, toString: function () { return "(" + this.left.toString() + "*" + this.right.toString() + ")"; } };
}
function lt(x, y) {
    return { type: LT, left: x, right: y, toString: function () { return "(" + this.left.toString() + "<" + this.right.toString() + ")"; } };
}
function and(x, y) {
    return { type: AND, left: x, right: y, toString: function () { return "(" + this.left.toString() + "&&" + this.right.toString() + ")"; } };
}
function not(x) {
    return { type: NOT, left: x, toString: function () { return "(!" + this.left.toString()+ ")"; } };
}
function ite(c, t, f) {
    return { type: ITE, cond: c, tcase: t, fcase: f, toString: function () { return "(if " + this.cond.toString() + " then " + this.tcase.toString() + " else " + this.fcase.toString() + ")"; } };
}

//Interpreter for the AST.
function interpret(exp, envt) {
    switch (exp.type) {
        case FALSE: return false;
        case NUM: return exp.val;
        case VR: return envt[exp.name];
        case PLUS: return interpret(exp.left, envt) + interpret(exp.right, envt);
        case TIMES: return interpret(exp.left, envt) * interpret(exp.right, envt);
        case LT: return interpret(exp.left, envt) < interpret(exp.right, envt);
        case AND: return interpret(exp.left, envt) && interpret(exp.right, envt);
        case NOT: return !interpret(exp.left, envt);
        case ITE: if (interpret(exp.cond, envt)) { return interpret(exp.tcase, envt); } else { return interpret(exp.fcase, envt); }
    }
}

function writeToConsole(text) {
    var csl = document.getElementById("console");
    if (typeof text == "string") {
        csl.value += text + "\n";
    } else {
        csl.value += text.toString() + "\n";
    }
}


// test a program for correctness according to
// examples
function initializePlist(vars, consts) {
  var plist = [flse()];
  //var plist = [];
  vars.forEach(v => plist.push(vr(v)));
  consts.forEach(n => plist.push(num(n)));
  return plist;
}

function isCorrect(program, examples) {
  var res = examples.map(x => interpret(program, x) === x._out);
  return !res.includes(false);
}

function equivalent(p1, p2, examples) {
  var res = examples.map(x => interpret(p1, x) === interpret(p2, x));
  return !res.includes(false);
}

function isNumerical(exp) {
  return [NUM, VR, PLUS, TIMES, ITE].includes(exp.type);
}

function isBoolean(exp) {
  return !isNumerical(exp);
}

function unary(exp, allowed) {
  var results = [];
  if(isBoolean(exp) && allowed.includes(NOT)) {
    results.push(
      not(exp)
    )
  }
  return results;
}

function binary(e1, e2, allowed, binAllowed) {
  var results = [];
  if(isNumerical(e1) && isNumerical(e2)) {
    //if(allowed.includes(PLUS))
      results.push(plus(e1, e2))
    //if(allowed.includes(TIMES))
      results.push(times(e1,e2))
    //if(allowed.includes(LT))
      results.push(lt(e1, e2), lt(e2, e1))
  }

  if(isBoolean(e1) && isBoolean(e2)) {
    if(binAllowed.includes(AND))
      results.push(and(e1, e2));
  }
  return results;
}

function ternary(e1, e2, e3, allowed) {
  var results = [];
  if(isBoolean(e1) && (isNumerical(e2) === isNumerical(e3))) {
    if(allowed.includes(ITE))
      results.push(
        ite(e1, e2, e3),
        ite(e1, e3, e2)
      );
  }
  return results;
}


function grow(plist, allowed, binAllowed) {
  // first all unary terms.
  var result = [];
  var i, j, k;
  for(i = 0; i < plist.length; i++) {
    result = result.concat(unary(plist[i], binAllowed));
  }
  // then all binary terms
  for(i = 0; i < plist.length; i++) {
    for(j = 0; j < plist.length; j++){
      result = result.concat(binary(plist[i], plist[j], allowed, binAllowed));
    }
  }
  // then all ternary terms
  for(i = 0; i < plist.length; i++) {
    for(j = 0; j < plist.length; j++){
      for(k = 0; k < plist.length; k++) {
        result = result.concat(ternary(plist[i], plist[j], plist[k], allowed));
      }
    }
  }
  return plist.concat(result);
}

// Calculates the probability of a given AST node, recursively.
function astProb(expr, prob) {
	switch(expr.type) {
		case FALSE: return 1
        case NUM: return 1
        case VR: return 1
        case PLUS: 
        case TIMES: 
        case LT: 
        case AND: 
        	return prob(expr.left.type, 0, expr.type) * prob(expr.right.type, 1, expr.type) * astProb(expr.left, prob) * astProb(expr.right, prob);
        case NOT: 
        	return prob(expr.left.type, 0, expr.type) * astProb(expr.left, prob)
        case ITE: 
        	return prob(expr.cond.type, 0, expr.type) * astProb(expr.cond, prob) * prob(expr.tcase.type, 1, expr.type) * astProb(expr.tcase, prob)
        	* prob(expr.fcase.type, 2, expr.type) * astProb(expr.fcase, prob)
	}
}

// forgive me 
Array.prototype.swap = function (x,y) {
  var b = this[x];
  this[x] = this[y];
  this[y] = b;
  return this;
}

function elimEquivalents(plist, examples, prob) {
  var i, j;
  for(i = 0; i < plist.length; i++) {
    for(j = i+1; j < plist.length; j++) {
      if(equivalent(plist[i], plist[j], examples)) {
      	if(astProb(plist[i], prob) < astProb(plist[j], prob))
      		plist.swap(i, j) // send the one with lower probability behind the barn to put a bullet between their eyes.
      	plist.splice(j, 1);
        j--;
      }
    }
  }
  return plist;
}

function bottomUp(globalBnd, intOps, boolOps, vars, consts, inputoutputs, prob) {
  var plist = initializePlist(vars, consts);
  console.log(plist.map(p => p.toString()));
  var i = 0, j;
  while(i < globalBnd) {
  	console.log("nivel ", i)
    plist = grow(plist, intOps, boolOps);
    console.log(plist.map(p => p.toString()));
    plist = elimEquivalents(plist, inputoutputs, prob);
    console.log(plist.map(p => p.toString()));

    for(j = 0; j < plist.length; j++) {
      if(isCorrect(plist[j], inputoutputs)){
      	console.log(astProb(plist[j], prob))
        return plist[j];
      }
    }
    i++;
  }
  return "FAIL";
}

function run2(){
	
	function prob(child, id, parent){
		//Example of a probability function. In this case, the function
		//has uniform distributions for most things except for cases that would
		//cause either type errors or excessive symmetries.
		//You want to make sure your solution works for arbitrary probability distributions.
		function unif(possibilities, kind){
			if(possibilities.includes(kind)){
				return 1.0/possibilities.length;
			}
			return 0;
		}
		
		switch(parent){
			case PLUS: 
				if(id == 0)
					return unif([NUM, VR, PLUS, TIMES, ITE], child);
				else
					return unif([NUM, VR, TIMES, ITE], child);
				break;
	        case TIMES: 
	        	if(id == 0)
					return unif([NUM, VR, PLUS, TIMES, ITE], child);
				else
					return unif([NUM, VR, ITE], child);
	        	break;	        	
	        case LT: 
	        	return unif([NUM, VR, PLUS, TIMES, ITE], child);
	        	break;
	        case AND:
	        	return unif([LT, AND, NOT], child);
	        	break;
	        case NOT:
	        	return unif([LT, AND], child);
	        	break;
	        case ITE:
	        	if(id == 0){
	        		return unif([LT, AND], child);					
				}else
					return unif([NUM, VR, PLUS, TIMES, ITE], child);
	        	break;
		}
	}
	
	
	var rv = bottomUp(2, [VR, NUM, PLUS, TIMES, ITE], 
			             [AND, NOT, LT, FALSE], ["x", "y"], [0, 1, 5], 
			             [{x: 0, y: 10, _out: 1}, 
			              {x: 6, y: 2, _out: 0}, 
			              {x: 2, y: 3, _out: 1}, 
			              {x: 7, y: 4, _out: 0}], 
			             prob
	);
	writeToConsole("RESULT: " + rv.toString());
	
}

